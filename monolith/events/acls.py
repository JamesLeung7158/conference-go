from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # pexels_url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    pexels_url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}

    pexels_headers = {"Authorization": PEXELS_API_KEY}
    pexels_r = requests.get(pexels_url, params=params, headers=pexels_headers)
    pexels_content = json.loads(pexels_r.content)
    try:
        return {"picture_url": pexels_content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    geocoding_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_r = requests.get(geocoding_url, params=geocoding_params)
    geocoding_content = geocoding_r.json()

    latitude = geocoding_content[0]["lat"]
    longitude = geocoding_content[0]["lon"]

    # try:
    #     data = {
    #         "latitude": geocoding_content[0]["lat"],
    #         "longitude": geocoding_content[0]["lon"],
    #     }
    # except (KeyError, IndexError):
    #     return None

    openweather_url = "https://api.openweathermap.org/data/2.5/weather"
    # openweather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&units=imperial"
    # openweather_params = {
    #     "lat": data["latitude"],
    #     "lon": data["longitude"],
    #     "appid": OPEN_WEATHER_API_KEY,
    #     # "units": "imperial",
    # }
    # openweather_headers = {"Authorization": OPEN_WEATHER_API_KEY}
    openweather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    openweather_r = requests.get(openweather_url, params=openweather_params)
    # openweather_r = requests.get(openweather_url, headers=openweather_headers)
    openweather_content = json.loads(openweather_r.content)
    try:
        return {
            "weather": {
                "temp": openweather_content["main"]["temp"],
                "description": openweather_content["weather"][0][
                    "description"
                ],
            }
        }
    except (KeyError, IndexError):
        return None
