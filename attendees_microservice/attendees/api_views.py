from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# from events.models import Conference
from .models import Attendee, ConferenceVO, AccountVO
from common.json import ModelEncoder


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created"]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


# class ConferenceListEncoder(ModelEncoder):
#     model = Conference
#     properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created", "conference"]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.
    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeesListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        print(content, "this is content")
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            print(conference_href, "this is conference_href")
            print(ConferenceVO.objects.all())
            conference = ConferenceVO.objects.get(import_href=conference_href)
            print(conference, "this is conference")
            content["conference"] = conference
            print(content["conference"], "this is content.conference")
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    # response = []
    # attendees = Attendee.objects.all()
    # for attendee in attendees:
    #     response.append(
    #         {
    #             "name": attendee.name,
    #             "email": attendee.email,
    #             "company_name": attendee.company_name,
    #             "created": attendee.created,
    #             "href": attendee.get_api_url(),
    #         }
    #     )

    # return JsonResponse({"attendees": response})


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.
    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    """
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, breakdown = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0, "breakdown": breakdown})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )

    # response = [
    #     {
    #         "name": attendee.name,
    #         "email": attendee.email,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "href": attendee.get_api_url(),
    #     }
    # ]
    # return JsonResponse({"attendee": response})
